"""Command Line interface for the TranscriptSampler class."""


def main():
    """A one line summary of the module or program, terminated by a period.

    Leave one blank line.  The rest of this docstring should contain an
    overall description of the module or program.  Optionally, it may also
    contain a brief description of exported classes and functions and/or usage
    examples.

    Typical usage example:

    foo = ClassFoo()
    bar = foo.FunctionBar()
    """
    from transcript_sampler import TranscriptSampler
    import argparse
    parser = argparse.ArgumentParser(description='Read functions argument')
    parser.add_argument('input', action='store',
                        type=str, help='name of the input file')
    parser.add_argument('n_reads', action='store',
                        type=int, help='number of total reads to be simulated')
    parser.add_argument('output', action='store',
                        type=str, help='name of the output file')
    args = parser.parse_args()
    smplr = TranscriptSampler()
    dc = smplr.read_avg_expression(args.input)
    dc_s = smplr.sample_transcripts(dc, args.n_reads)
    smplr.write_sample(args.output, dc_s)


if __name__ == '__main__':
    main()
