"""A one line summary of the module or program, terminated by a period.

Leave one blank line.  The rest of this docstring should contain an
overall description of the module or program.  Optionally, it may also
contain a brief description of exported classes and functions and/or usage
examples.

  Typical usage example:

  foo = ClassFoo()
  bar = foo.FunctionBar()
"""
from random import choices


def read_avg_expression(file):
    """Reads a gene expression summary file.

    Reads through a gene expression file with the format gene_id nr_copies.
    Stores the values of every row in a dictionary and returns the dictionary.

    Args:
        file (string): name of the gene expression summary file.

    Returns:
        dictionary: dictionary storing the information of the gene
        expression summary file.
    """
    myfile = open(file, 'r')
    # initialize empty dictionary
    dc = {}
    # read line and split key-value
    for myline in myfile:
        d = myline.split(' ')
    # assing key and value to the dictionary and strip \n character.
        dc[d[0].strip()] = int(d[1].strip())
    myfile.close()
    return dc


def sample_transcripts(avgs, number):
    """Simulates a RNAseq experiment with provided gene relative abundances.

    Extracts the count data from a dictionary and calculates the relative
    abundances of each gene. It samples a defined number of reads following
    the calculated relative abundances and return a dictionary with the
    sampled reads.

    Args:
        avgs (dictionary): name of the dictionary storing the gene expression
        counts.
        number (int): number of the transcripts to be sampled.

    Returns:
        dictionary: dictionary storing the sampled transcript counts.
    """
    # extract count numbers
    val = list(avgs.values())
    # calculate relative abundance
    tot = sum(val)
    rel_val = [x/tot for x in val]
    # sampling
    smpl = choices(list(avgs.keys()), weights=rel_val, k=number)
    # initialize empty dictionary
    smpl_dc = dict()
    # count the genes occurence
    for i in smpl:
        if i not in smpl_dc:
            smpl_dc[i] = 1
        else:
            smpl_dc[i] += 1
    # return the new dictionary
    return smpl_dc


def write_sample(file, sample):
    """Writes a gene expression summary file.

    Writes the RNA counts data contained in a dictionary to a file in the
    format gene_id nr_copies.

    Args:
        file (string): name of the file to be written.
        sample(dictionary): dictionary containing the RNA counts data.
    """
    # open file in write mode
    myfile = open(file, 'w')
    for (k, v) in sample.items():
        # join each key value pair with a blankspace
        ln = ' '.join([k, str(v)])
        # write a new line
        myfile.write(ln + '\n')
    myfile.close()
