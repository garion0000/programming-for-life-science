import pytest

from code.transcript_sampler import TranscriptSampler

smplr= TranscriptSampler()

def test_read_avg_expression():
    dc=smplr.read_avg_expression('./test/genes.txt')
    assert type(smplr.read_avg_expression('./test/genes.txt'))==dict
    assert smplr.read_avg_expression('./test/genes.txt')=={'GENE1': 432, 'GENE2': 89332, 'GENE3': 14, 'GENE4': 3432, 'GENE5': 2398}
    with pytest.raises(IOError):
        smplr.read_avg_expression('file_not_existing.txt')
        
        
def test_sample_transcripts():
    dc=smplr.read_avg_expression('./test/genes.txt')
    assert type(smplr.sample_transcripts(dc,111))==dict
    tot = 0
    dc2 = smplr.sample_transcripts(dc,111)
    for key in dc2.keys():
        tot += dc2[key]
    assert tot == 111


    
    
def test_write_sample():
    dc=smplr.read_avg_expression('./test/genes.txt')
    smplr.write_sample('./test/original_output.txt',dc)
    dc2 = smplr.sample_transcripts(dc,111)
    smplr.write_sample('./test/sampled_output.txt',dc2)
    assert smplr.read_avg_expression('./test/original_output.txt')==dc
    assert smplr.read_avg_expression('./test/sampled_output.txt')==dc2